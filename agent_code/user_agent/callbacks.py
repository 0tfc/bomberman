from agent_code.noTime_agent.callbacks import state_to_features, CoinCounter

def setup(self):
    self.coin_counter = CoinCounter()
    self.own_bomb = None  # (x, y)
    self.own_bomb_countdown = 0
    pass


def act(self, game_state: dict) -> str:
    self.logger.info('Pick action according to pressed key')

    self.coin_counter.update(game_state)

    if self.own_bomb_countdown <= 0:
        if (game_state["self"][3], 3) in game_state["bombs"]:
            self.own_bomb, self.own_bomb_countdown = game_state["self"][3], 4
        else:
            self.own_bomb = None
    if self.own_bomb_countdown > 0:
        self.own_bomb_countdown -= 1

    print("user", state_to_features(game_state, self.coin_counter.current,
                                    self.own_bomb))
    return game_state['user_input']
