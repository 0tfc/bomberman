from collections import namedtuple, deque
from typing import List, Generator, Tuple
import numpy as np
from datetime import datetime
import os
import json

import events as e
from . import callbacks as c
from . import policies as p
from . import features as f
from . import custom_events as ce

# learning rate
ALPHA = 0.5
# discount factor
GAMMA = 0.05 #important for training with other opponents !

GAME_REWARDS = {
    e.COIN_COLLECTED: 1,
    # e.KILLED_OPPONENT: 5, # combined with n-step learning: could be reasonable
    #e.KILLED_SELF: -42,
    # e.CRATE_DESTROYED: 0.1, # combined with n-step learning: could be reasonable
    #e.INVALID_ACTION: -0.1,
    # ce.RUN_INTO_DEATH: -30,
    ce.BAD_MOVE: -30,  # possibly run into death
    ce.WAITED_FOR_DEATH: -30,
    ce.TARGET_ATTACKED: 0.2,
    ce.BAD_BOMB: -30,  # possibly run into death
    # ce.UNNECESSARY_BOMB: -2,
    ce.NOT_MOVED_TO_BETTER_BOMB_POS: -0.2,
    ce.MOVED_TO_BETTER_BOMB_POS: 0.1,
    ce.NOT_MOVED_TO_COIN: -0.4,
    ce.MOVED_TO_COIN: 0.2,
    ce.USELESS_MOVE: -0.2,
    ce.USEFUL_MOVE: 0.1,
    ce.OPPONENT_ATTACKED: 0.5,
    ce.GOOD_ATTACK: 5,
    ce.RUN_INTO_DANGER: -0.5,
    ce.ESCAPE_FROM_DANGER: 0.25,
    ce.KAMIKAZE_HERO: 30,
    ce.KAMIKAZE_INSUBORDINATOR: -10

    # later
    # plant_bomb_in_near_of_opponent
}

OBSERVABLES = [
    ("total_steps", lambda state, agent: state["step"]),
    ("total_move", lambda state, agent: sum(a < 4 for a in agent.a_t)),
    ("avg_reward",
     lambda state, agent: sum(agent.rewards_t)/len(agent.rewards_t)),
    ("total_score", lambda state, agent: state["self"][1])
]

def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # define a container for the rewards r_tau,t during one episode r_tau
    self.rewards_t = []
    self.a_t = []
    self.Q_t = []
    self.s_t = []


    # Policies Hyperparameters

    epsilon_greedy_parameters = {
        "initial_random_prob": 0.99,
        "random_prob_decay_factor": 0.0025,  # define how fast the probability shall decrease per episode
        "random_prob_min": 0.05
    }

    softmax_parameters = {
        # parameter for softmax policy and annealing of the temperature
        "initial_rho":  0.01, # starting value
        "rho_decay_factor":  0.0,
        "rho_min":  0.01
    }

    self.train_policy = p.EpsilonGreedy(**epsilon_greedy_parameters)
    self.test_policy = p.Greedy() # note these test runs were also used for updating the q-values
    self.test_period = 25
    self.validation_policy = p.Softmax(**softmax_parameters)
    self.pretrain_mode2 = 0  # 10000

    self.save_observables_for_report = True

    if self.save_observables_for_report:
        self.model_name = datetime.now().isoformat().split('.')[0]
        self.model_name = os.path.join("task3", self.model_name)
        os.makedirs(os.path.join("data", self.model_name))
        self.observables = {
            collection: {
                name: []
                for name, _ in OBSERVABLES
            }
            for collection in ["train", "test", "validation"]
        }
        with open(os.path.join("data", self.model_name, "parameters.json"),
                  "w") as file:
            json.dump({
                "ALPHA": ALPHA,
                "GAMMA": GAMMA,
                "GAME_REWARDS": GAME_REWARDS,
                "test_policy": self.test_policy.parameters,
                "test_period": self.test_period,
                "validation_policy": self.validation_policy.parameters,
                "train_policy": self.train_policy.parameters,
                "pretrained": self.use_pretrained and os.path.isfile("Q_table.npy"),
                "mode2_pretrained_until": self.pretrain_mode2
            }, file)


def transformations(index: int, pretrain_mode2: bool) \
 -> Generator[Tuple[np.array, int, bool, bool], None, None]:
    features = f.get_features_from_index(index)
    new_features = features.copy()
    for rotation in range(4):
        for mirrored in [False, True]:
            for mode2 in ([False, True]
                          if pretrain_mode2 and new_features[5] != 2
                          else [False]):
                new_features[:4] = np.roll(features[:4], rotation)
                if mirrored:  # mirror at horizontal axis (up <-> down)
                    new_features[:4] = np.array([new_features[2],
                                                 new_features[1],
                                                 new_features[0],
                                                 new_features[3]])
                    if mode2:
                        new_features[5] = 2
                yield new_features, rotation, mirrored, mode2


def transformed_action(action: int, rotation: int, mirrored: bool) -> int:
    transformed_actions = np.arange(4)
    if mirrored:
        transformed_actions = [transformed_actions[i]
                               for i in [2, 1, 0, 3]]
    transformed_actions = np.roll(transformed_actions, -rotation).tolist()
    transformed_actions += [4,5]
    return transformed_actions[action]


def game_events_occurred(self, old_game_state: dict, self_action: str,
                         new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """

    # add custom events to events list
    events += events_custom(self, old_game_state, events, self_action)

    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    # DO THE STEP 1 a) of the lecture


    # PROBLEM INDEX CONFUSION S_t, a_t, are there more states than rewards and actions and Q-values !!!!!
    if self_action != None: # self action describes the action that you took. at the beginning you take no action
        # store the actual state_index # TODO: probably wrong state!
        self.s_t.append(f.get_index_from_features(c.state_to_features(old_game_state, self.coin_counter.current, self.own_bomb)))
        # store the reward r_tau,t (need them for step b) )
        self.rewards_t.append(reward_from_events(self,events))
        # store the action a_tau,t (need them for step b) )
        self.a_t.append(c.ACTIONS.index(self_action))
        # store the Q-value of the previous state s_tau,t-1 and action a_tau,t-1
        self.Q_t.append(self.Q_table[self.s_t[-1]][self.a_t[-1]])


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    #Note: last game state is not the last one

    # add custom events to events list
    custom_events = events_custom(self, last_game_state, events, last_action)
    events += custom_events

    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')

    # store last values:
    # store final reward
    self.rewards_t.append(reward_from_events(self,events))
    # store final action_index
    #self.a_t.append(c.ACTIONS.index(last_action))
    # DONT STORE LAST ACTION BECAUSE FOR LAST ACTION ONE CANNOT UPDATE THE Q-TABLE,
    # SINCE THE FUTURE Q-VALUE (V-VALUE) DONT EXIST
    # store the final state_index
    self.s_t.append(f.get_index_from_features(c.state_to_features(last_game_state, self.coin_counter.current, self.own_bomb)))
    # store the Q-value of the last state s_tau,T_tau and a_tau,T_tau
    self.Q_t.append(self.Q_table[self.s_t[-1]][self.a_t[-1]])

    self.Q_table_old = self.Q_table

    # Store the model
    #PLEASE CHECK!!!!!!!!
    for t in range(len(self.a_t)):
        # Q-learning:
        v_t = np.max(self.Q_table_old[self.s_t[t+1]])
        # Sarsa:
        # v_t = self.Q_table_old[self.s_t[t+1]][self.a_t[t+1]]
        # v_t = self.Q_t[self.s_t[t+1]][self.a_t[t+1]]

        # use the rotational and mirroring invariance of the game, i.e. update for
        # each state the corresponding rotated/mirrored states with the corresponding
        # action and the same reward
        for transformed_features, rotation, mirrored, mode2 \
         in transformations(self.s_t[t],
                            self.pretrain_mode2 < last_game_state["round"]):
            s_i_prime = f.get_index_from_features(transformed_features)
            a_t_prime = transformed_action(self.a_t[t], rotation, mirrored)


            self.Q_table[s_i_prime][a_t_prime] = \
                self.Q_table_old[s_i_prime][a_t_prime] \
                + ALPHA*(self.rewards_t[t] \
                         +GAMMA*v_t-self.Q_table_old[s_i_prime][a_t_prime])


        # old implementation w/o using rotational and mirroring invariance
        #self.Q_table[self.s_t[t]][self.a_t[t]] = self.Q_table_old[self.s_t[t]][self.a_t[t]] + ALPHA*(self.rewards_t[t]+GAMMA*v_t-self.Q_table_old[self.s_t[t]][self.a_t[t]])

        # variant: n-step Q-learning... DO THIS
    if self.save_observables_for_report:
        parameters_file = os.path.join("data", self.model_name, "parameters.json")
        with open(parameters_file) as file:
            parameters = json.load(file)
        if "players" not in parameters:
            with open(parameters_file, "w") as file:
                players = [name for name in self.coin_counter.current.scores]
                json.dump({**parameters, "players": players}, file)

    if self.save_observables_for_report:
        np.save(os.path.join("data", self.model_name, "Q_table.npy"), self.Q_table)
    else:
        np.save("Q_table.npy", self.Q_table)
    # manipulation of last_game_state to honour score achieved in the last step
    corrected_self = list(last_game_state["self"])
    for event in events:
        if e.COIN_COLLECTED == event:
            corrected_self[1] += 1
        elif e.KILLED_OPPONENT == event:
            corrected_self[1] += 5
    last_game_state["self"] = tuple(corrected_self)

    if last_game_state["round"] % self.test_period == 0:
        collection = "test"
        print(f"""
 Episode: {last_game_state["round"]:7d} ({self.test_policy.parameters["name"]}\
policy run)
-------------------------------------""")
        self.test_policy.print_current_parameter()
        self.test_policy.update()
        if last_game_state["self"][1] < 7:
            with open(os.path.join("data", self.model_name,
                     f"{collection}-s_t.jsonl"), "a") as file:
                json.dump({
                    "episode": last_game_state["round"],
                    "score": last_game_state["self"][1],
                    "states": [f.get_features_from_index(i).tolist()
                               for i in self.s_t[int(len(self.s_t)*0.9):]]
                }, file)
    elif last_game_state["round"] % self.test_period == 1:
        collection = "validation"
        print(f"""
 Episode: {last_game_state["round"]:7d} \
({self.validation_policy.parameters["name"]} policy run)
-------------------------------------""")
        self.validation_policy.print_current_parameter()
        self.validation_policy.update()
    else:
        collection = "train"
        print(f"""
 Episode: {last_game_state["round"]:7d}
--------------------------------------""")
        self.train_policy.print_current_parameter()
        self.train_policy.update()

    for name, function in OBSERVABLES:
        value = function(last_game_state, self)
        print(f"{name:>12}: {value:6.2f}".replace(".00", ""))
        if self.save_observables_for_report:
            self.observables[collection][name].append(value)
            np.save(os.path.join("data", self.model_name,
                                 f"{collection}-{name}.npy"),
                    np.array(self.observables[collection][name]))

    # reset values for next episode
    self.rewards_t = []
    self.a_t = []
    self.s_t = []
    self.Q_t = []

    self.own_bomb = None


def reward_from_events(self, events: List[str]) -> int:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """

    reward_sum = 0
    for event in events:
        if event in GAME_REWARDS:
            reward_sum += GAME_REWARDS[event]
    self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
    # print(f"Awarded {reward_sum} for events {', '.join(events)}")
    return reward_sum



# Functions-------------------------------------------------------------------


# CAUTION if feature = 1 meens another thing than we have to redefine this!!!!
def events_custom(self, old_game_state: dict, events: List[str], action: str) -> List[str]:
    if old_game_state is None:
        return []

    custom_events = []

    # print("calling state_to_features from act")
    old_features = c.state_to_features(old_game_state, self.coin_counter.current, self.own_bomb)
    # Idea: Add your own events to hand out rewards

    # define a event which says whether the agent runned into the safe death
    if action in c.ACTIONS[:4] and old_features[c.ACTIONS[:4].index(action)] == 2:
        custom_events.append(ce.BAD_MOVE)
    if action in ["BOMB", "WAIT"] and old_features[4] == 4:
        custom_events.append(ce.WAITED_FOR_DEATH)

    if 1 in old_features[:4]:
        if action in c.ACTIONS[:4]:
            if old_features[c.ACTIONS[:4].index(action)] == 1:  # only works if 0-3: content of upper, right, lower and left field and if 1 means that you go to Coin
                if old_features[f.MODE] == 0:
                    custom_events.append(ce.MOVED_TO_COIN)
                elif old_features[f.MODE] == 1:
                    custom_events.append(ce.MOVED_TO_BETTER_BOMB_POS)
            else:
                if old_features[f.MODE] == 0:
                    custom_events.append(ce.NOT_MOVED_TO_COIN)
                elif old_features[f.MODE] == 1:
                    custom_events.append(ce.NOT_MOVED_TO_BETTER_BOMB_POS)
        elif action == "WAIT":
            if old_features[f.MODE] == 0:
                custom_events.append(ce.NOT_MOVED_TO_COIN)
            elif old_features[f.MODE] == 1:
                custom_events.append(ce.NOT_MOVED_TO_BETTER_BOMB_POS)

    # introduce good bomb GOOD_BOMB_PLACEMENT
    # weight a bomb placement regarding the number of targeted crates/opponentes
    if action == "BOMB":
        n_targets = c.attackable_crates_oppenents(
            *c.state_variables(old_game_state))
        custom_events += [ce.TARGET_ATTACKED] * n_targets
        n_opponents = c.attackable_crates_oppenents(
            *c.state_variables(old_game_state), count_crates = False)
        custom_events += [ce.OPPONENT_ATTACKED] * n_opponents
        if old_features[f.CURRENT_FIELD] == 3 and n_targets < 6: # kill'em case
            custom_events.append(ce.GOOD_ATTACK)
        if old_features[f.CURRENT_FIELD] == 0:
            custom_events.append(ce.BAD_BOMB)

    if 1 in old_features[:4] or old_features[f.CURRENT_FIELD] in [1, 2, 3]:
        if action in c.ACTIONS[:4]:
            if old_features[:4][c.ACTIONS[:4].index(action)] != 1:
                custom_events.append(ce.USELESS_MOVE)
            else:
                custom_events.append(ce.USEFUL_MOVE)
        elif action == "WAIT":
            custom_events.append(ce.USELESS_MOVE)

    if 3 in old_features[:4] and any(i in old_features[:4] for i in [0, 1]):
        if action in c.ACTIONS[:4]:
            if old_features[:4][c.ACTIONS[:4].index(action)] == 3:
                custom_events.append(ce.RUN_INTO_DANGER)
            else:
                custom_events.append(ce.ESCAPE_FROM_DANGER)
        elif old_features[4] != 4:
            custom_events.append(ce.ESCAPE_FROM_DANGER)

    # reward for kamikaze
    if all(old_features == np.array([2]*f.N_FEATURES)):
        if action == "BOMB":
            custom_events.append(ce.KAMIKAZE_HERO)
        else:
            custom_events.append(ce.KAMIKAZE_INSUBORDINATOR)


    return custom_events
