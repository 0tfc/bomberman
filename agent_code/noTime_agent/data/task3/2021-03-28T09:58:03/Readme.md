- non-pretrained
- no mode2 pretraining
- 50k epochs
- opponents:
  - 1 rule-based agent
  - 1 peaceful agent

Plan:
- Repeat this experiment using the Q-table from task2-setting from 2021-03-27T23:23:21 and use the better one as pretraining for the tournament.
- Use the better one as pretrained bases for another 50k training epochs with 3 rule-based agents.
