- non-pretrained
- full mode2 pretraining
- 100k epochs
- opponents: non

Plan:
- Use this task2-setting from 2021-03-27T23:23:21 as pre-ipretraining for the tournament.
- Another 50k pretraining with 1 rule-based and 1 peaceful agent.
- Finally another 50k epochs with 3 rule-based agents.
