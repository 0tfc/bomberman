import os
import numpy as np
from typing import List, Tuple, Iterable

from ..noTime_agent import callbacks as ntc
from ..noTime_agent import train as ntt
from ..noTime_agent import features as f
from ..noTime_agent import policies as p
import settings as s

BombsType = List[Tuple[Tuple[int, int], int]]
DIRECTIONS = [(0, -1), (1, 0), (0, 1), (-1, 0)]  # up, right, down, left
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup(self):
    self.coin_counter = ntc.CoinCounter()
    n_states = f.N_STATES
    n_actions = 6
    self.Q_table = np.random.rand(n_states, n_actions)
    for i in range(f.N_STATES):
        self.Q_table[i] = get_q(f.get_features_from_index(i))
    self.policy = p.Greedy()


def get_q(features: np.array) -> np.array:
    q = np.zeros(f.N_FEATURES)
    q[:4][features[:4] == 1] = 1
    q[:4][features[:4] == 2] = -1
    q[:4][features[:4] == 3] = -0.5
    if features[4] == 4:
        q[5] = -1
    else:
        q[5] = features[4] * 1.1
        if all(q[:4] == 2):
            q[4] = 1
    return q


def act(self, game_state: dict) -> str:
    """
    Your agent should parse the input, think, and take a decision.
    When not in training mode, the maximum execution time for this method is 0.5s.

    :param self: The same object that is passed to all of your callbacks.
    :param game_state: The dictionary that describes everything on the board.
    :return: The action to take as a string.
    """
    # update the number of collectable coins
    self.coin_counter.update(game_state)

    # get from state the current feature
    # print("calling state_to_features from act")
    current_features = ntc.state_to_features(game_state, self.coin_counter.current)
    # get from feature the index
    feature_index = f.get_index_from_features(current_features)

    action = self.policy.act(self, feature_index)
    print("secret", current_features, action)
    custom_events = ntt.events_custom(self, game_state, [], action)
    reward = sum(ntt.GAME_REWARDS[event] for event in custom_events)
    print("→", reward, "from", custom_events)

    return action
