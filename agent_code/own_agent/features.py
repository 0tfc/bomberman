# 0: int - minimum distance to a coin
# 1: int - content of the left neighbouring tile
# 2: int - content of the upper neighbouring tile
# 3: int - content of the bottom neighbouring tile
# 4: int - content of the right neighbouring tile
# 5: int - is left field nearer to nearest coin and is a free field (0=no, 1=yes)
# 6: int - is upper field nearer to nearest coin and is a free field (0=no, 1=yes)
# 7: int - is bottom field nearer to nearest coin and is a free field (0=no, 1=yes)
# 8: int - is right field nearer to nearest coin and is a free field (0=no, 1=yes)

MIN_COIN_DISTANCE = 0
CONTENT_LEFT, CONTENT_UP, CONTENT_DOWN, CONTENT_RIGHT = 1, 2, 3, 4
NEAREST_COIN_LEFT, NEAREST_COIN_UP, NEAREST_COIN_DOWN, NEAREST_COIN_RIGHT = 5, 6, 7, 8
N_FEATURES = 9
