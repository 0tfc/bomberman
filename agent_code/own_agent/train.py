import pickle
import random
from collections import namedtuple, deque
from typing import List

import events as e
from .callbacks import state_to_features

#--------------------------own--------------------------------------------------
import settings as s
import agent_code.own_agent.features as f
import agent_code.own_agent.custom_events as ce
#-------------------------------------------------------------------------------

# This is only an example!
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

# Hyper parameters -- DO modify
TRANSITION_HISTORY_SIZE = 3  # keep only ... last transitions
RECORD_ENEMY_TRANSITIONS = 1.0  # record enemy transitions with probability ...

# Events
PLACEHOLDER_EVENT = "PLACEHOLDER"

def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # Example: Setup an array that will note transition tuples
    # (s, a, r, s')
    self.transitions = deque(maxlen=TRANSITION_HISTORY_SIZE)

    self.total_rewards = 0 # just for us to know whether training is working

def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    # Idea: Add your own events to hand out rewards
    #if ...:
    #    events.append(PLACEHOLDER_EVENT)

    # state_to_features is defined in callbacks.py
    #self.transitions.append(Transition(state_to_features(old_game_state), self_action, state_to_features(new_game_state), reward_from_events(self, events)))

    #---------------------------------------------------------------------------
    if old_game_state is None:
        return

    events += events_from_state_transition(self, old_game_state, new_game_state)
    reward = reward_from_events(self, events)
    #terminal = e.GOT_KILLED in events or e.KILLED_SELF in events or steps == 400
    self.total_rewards += reward
    print("step reward", reward)
    self.agent.observe(reward, terminal=False) #this update the agent
    #---------------------------------------------------------------------------

def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    self.transitions.append(Transition(state_to_features(last_game_state), last_action, None, reward_from_events(self, events)))

    # Store the model
    #with open("my-saved-model.pt", "wb") as file:
    #    pickle.dump(self.model, file)

    #---------------------------------------------------------------------------
    reward = reward_from_events(self, events)
    self.agent.observe(reward, terminal=True)

    self.total_rewards += reward
    print("total reward", self.total_rewards)
    print("avg. reward:", self.total_rewards/last_game_state["step"])
    self.total_rewards = 0 # reset counter for next episode
    # save the updates
    self.agent.save("model", "model")
    #---------------------------------------------------------------------------

def events_from_state_transition(self, old_game_state: dict, new_game_state: dict) -> List[str]:
    custom_events = []

    # reward for decreasing the distance
    new_features = state_to_features(new_game_state)
    old_features = state_to_features(old_game_state)

    if new_features[f.MIN_COIN_DISTANCE] >= old_features[f.MIN_COIN_DISTANCE]:
        custom_events.append(ce.NOT_MOVED_TO_COIN)
    else:
        custom_events.append(ce.MOVED_TO_COIN)

    return custom_events

def reward_from_events(self, events: List[str]) -> float:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """
    game_rewards = {
        e.COIN_COLLECTED: 1.1, #account for negative reward when increasing distance
                               #to the next nearest coin
        e.KILLED_OPPONENT: 5,
        e.KILLED_SELF: -42,
        e.INVALID_ACTION: -0.3,
        ce.NOT_MOVED_TO_COIN: -0.1,
        ce.MOVED_TO_COIN: 0.1
    }
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]
    self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
    return reward_sum
