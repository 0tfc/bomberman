import os
import pickle
import random

import numpy as np

#-----------------------------own-----------------------------------------------
import settings as s
import agent_code.own_agent.features as f
from tensorforce.environments import Environment
from tensorforce.agents import Agent
from tensorforce.execution import Runner
from typing import List, Tuple
#-------------------------------------------------------------------------------


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


class BomberRLeEnvironment(Environment):
    def __init__(self):
        super().__init__()

    def states(self):
        return dict(type='float', shape=(f.N_FEATURES,), min_value=-1, max_value=30)

    def actions(self):
        return dict(type='int', num_values=5)

    def max_episode_timesteps(self):
        return s.MAX_STEPS

def setup(self):
    """
    Setup your code. This is called once when loading each agent.
    Make sure that you prepare everything such that act(...) can be called.

    When in training mode, the separate `setup_training` in train.py is called
    after this method. This separation allows you to share your trained agent
    with other students, without revealing your training code.

    In this example, our model is a set of probabilities over actions
    that are is independent of the game state.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    #if self.train or not os.path.isfile("my-saved-model.pt"):
    #    self.logger.info("Setting up model from scratch.")
    #    weights = np.random.rand(len(ACTIONS))
    #    self.model = weights / weights.sum()
    #else:
    #    self.logger.info("Loading model from saved state.")
    #    with open("my-saved-model.pt", "rb") as file:
    #        self.model = pickle.load(file)

    self.environment = Environment.create(environment=BomberRLeEnvironment)
    if not os.path.isfile("model/checkpoint"):
        self.agent = Agent.create(agent='agent.json', environment=self.environment)
    else:
        # load trained parameters
        self.agent = Agent.load("model", "checkpoint", "checkpoint", environment=self.environment,
            agent='agent.json')


def act(self, game_state: dict) -> str:
    """
    Your agent should parse the input, think, and take a decision.
    When not in training mode, the maximum execution time for this method is 0.5s.

    :param self: The same object that is passed to all of your callbacks.
    :param game_state: The dictionary that describes everything on the board.
    :return: The action to take as a string.
    """
    # TODO: Exploration vs exploitation
    #random_prob = .1
    #if self.train and random.random() < random_prob:
    #    self.logger.debug("Choosing action purely at random.")
    #    # 80%: walk in any direction. 10% wait. 10% bomb.
    #    return np.random.choice(ACTIONS, p=[.2, .2, .2, .2, .1, .1])

    #self.logger.debug("Querying model for action.")
    #return np.random.choice(ACTIONS, p=self.model)
    states = state_to_features(game_state)
    action = self.agent.act(states)

    ## TODO:

    # for not training case:
    # If you want to use the greedy policy, you can use act(..., deterministic=True),
    # and if you do not train anymore (hence you don't need to call observe() after act()),
    # you can additionally set act(..., independent=True), so that the model doesn't expect a follow-up observe().

    # add parallel = 4 or something to do parallel computing

    return ACTIONS[action]

def state_to_features(game_state: dict) -> np.array:
    """
    *This is not a required function, but an idea to structure your code.*

    Converts the game state to the input of your model, i.e.
    a feature vector.

    You can find out about the state of the game environment via game_state,
    which is a dictionary. Consult 'get_state_for_agent' in environment.py to see
    what it contains.

    :param game_state:  A dictionary describing the current game board.
    :return: np.array
    """
    # This is the dict before the game begins and after it ends
    if game_state is None:
        return None

    # For example, you could construct several channels of equal shape, ...
    features = np.zeros(f.N_FEATURES)
        # 0: bool - if a coin is visible
    # 0: int - minimum distance to a coin
    # 1: int - content of the left neighbouring tile
    # 2: int - content of the upper neighbouring tile
    # 3: int - content of the bottom neighbouring tile
    # 4: int - content of the right neighbouring tile
    # 5: int - is left field nearer to nearest coin and is a free field (0=no, 1=yes)
    # 6: int - is upper field nearer to nearest coin and is a free field (0=no, 1=yes)
    # 7: int - is bottom field nearer to nearest coin and is a free field (0=no, 1=yes)
    # 8: int - is right field nearer to nearest coin and is a free field (0=no, 1=yes)


    #----------------------Compute Agent-Coin-Distance-------------------------
    # at first compute the distance of the agent to the next nearest coin

    coins_position = game_state["coins"]
    #coin_visible = len(coins_position) != 0

    #channels.append(coin_visible)

    own_x, own_y = game_state["self"][3]

    features[f.MIN_COIN_DISTANCE] = min_coin_distance(own_x, own_y, coins_position)

    # neigbouring fiels:
        # 0 if free field
        # 1 wall
        # 2 crate ← wegballern
        # 3 opponent ← wegballern, fürchten
        # 4 bomb ← fürchten

    #----------------------Compute Field--Map-------------------------
    field_map = game_state["field"]
    field_map[field_map == 1] = 2
    field_map[field_map == -1] = 1
    for pos, countdown in game_state["bombs"]:
        x, y = pos
        field_map[x, y] = 4
    for name, score, can_place_bomb, pos in game_state["others"]:
        x, y = pos
        field_map[x, y] = 3
    features[f.CONTENT_LEFT:f.CONTENT_LEFT+4] = field_map[own_x-1:own_x+2,own_y-1:own_y+2].flat[1::2]

    #neighboring fields:
        # -if the field is free the field has the value of the next nearest coin Distance, only if there are coins coin_visible
        #  otherwise free fields contains the value 30
        # -if the field is a wall the field has the value of 100
        # -if the field is a crate the field has the value of 50

    #neighbouring_fields = []

    helper_x = [-1,0,0,1]
    helper_y = [0,-1,1,0]
    #for i in range(4):
        #x, y = own_x + helper_x[i], own_y + helper_y[i]
        #content = game_state["field"][x, y]
        #if content == -1: # wall tile
        #    neighbouring_fields.append(100)
        #elif content == 1:
        #    neighbouring_fields.append(50)
        #else:
        #    neighbouring_fields.append(min_coin_distance(x,y,coins_position))

    #channels += neighbouring_fields

    current_dist = min_coin_distance(own_x,own_y,coins_position)
    for i in range(4):
        x, y = own_x + helper_x[i], own_y + helper_y[i]
        if min_coin_distance(x, y, coins_position) < current_dist and \
            features[f.CONTENT_LEFT+i] == 0:
            features[f.NEAREST_COIN_LEFT+i] = 1
        else:
            features[f.NEAREST_COIN_LEFT+i] = 0

    if np.sum(features[f.NEAREST_COIN_LEFT:f.NEAREST_COIN_RIGHT+1]) == 0:
        print("ERROR: coin feature is only 0 0 0 0")
    #TODO Bomb feature --> agent should know where they are and to avoid them
    return features

def min_coin_distance(own_x: int, own_y: int, coins: List[Tuple[int]]) -> int:
    if len(coins) == 0:
        return 30
    return min(
        abs(coin_x - own_x) + abs(coin_y - own_y)
        + (2 if (coin_x == own_x and coin_x % 2 == 0 and coin_y != own_y
        or coin_y == own_y and coin_y % 2 == 0 and coin_x != own_x) else 0)
        for coin_x, coin_y in coins)
